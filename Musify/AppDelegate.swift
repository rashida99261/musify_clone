//
//  AppDelegate.swift
//  Musify
//
//  Created by Rashida on 07/02/20.
//  Copyright © 2020 Rashida. All rights reserved.
//

import UIKit
import AudioToolbox
import AVFoundation
import UserNotifications


let APPLE_LANGUAGE_KEY = "AppleLanguages"
var currentLang : String = ""
var userDef = UserDefaults.standard
var player: SPTAudioStreamingController?

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,AVAudioPlayerDelegate, AlarmApplicationDelegate {

    var window: UIWindow?
     var auth = SPTAuth()

    var audioPlayer: AVAudioPlayer?
    let alarmScheduler: AlarmSchedulerDelegate = Scheduler()
    var alarmModel: Alarms = Alarms()
    
    var backgroundUpdateTask: UIBackgroundTaskIdentifier!
       
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
      //  MusicService.configure()
        
        UNUserNotificationCenter.current().delegate = self
        
        auth.redirectURL     = URL(string: "Spotify-Demo://returnAfterLogin") // insert your redirect URL here
        auth.sessionUserDefaultsKey = "current session"
       
        let langArray = userDef.object(forKey: APPLE_LANGUAGE_KEY) as! NSArray
        let lang = langArray.firstObject as! String
        if(lang == "en-GB")
        {
           // currentLang = "0"
        }
        else
        {
           //L012Localizer.DoTheSwizzling()
           //LocalizedClass.setAppleLAnguageTo(lang: "de")
        }
        
       // setDEfaultLanGerman()
        
        
        let session = AVAudioSession.sharedInstance()
        do{
            try session.setActive(true)
            try session.setCategory(.playback, mode: .default,  options: .defaultToSpeaker)
           
            
        } catch{
            print(error.localizedDescription)
        }
        
        window?.tintColor = UIColor.red
        
        return true
    }
    
  
   
    
    //snooze notification handler when app in background
    func application(_ application: UIApplication, handleActionWithIdentifier identifier: String?, for notification: UILocalNotification, completionHandler: @escaping () -> Void) {
        var index: Int = -1
        var soundName: String = ""
        if let userInfo = notification.userInfo {
            soundName = userInfo["soundName"] as! String
            index = userInfo["index"] as! Int
        }
        self.alarmModel = Alarms()
        self.alarmModel.alarms[index].onSnooze = false
        if identifier == Id.snoozeIdentifier {
            alarmScheduler.setNotificationForSnooze(snoozeMinute: 2, soundName: soundName, index: index)
            self.alarmModel.alarms[index].onSnooze = true
        }
        completionHandler()
    }
    
    //print out all registed NSNotification for debug
    func application(_ application: UIApplication, didRegister notificationSettings: UIUserNotificationSettings) {
        
        print(notificationSettings.types.rawValue)
    }
    
    //AlarmApplicationDelegate protocol
    func playSound(_ soundName: String) {
        
        
        if soundName.hasPrefix("file:///") { // soundName
            
            let audioUrl = URL(string: soundName)
            
            let documentsPath = FileManager.default.urls(for: .libraryDirectory, in: .userDomainMask)[0]
            let docUrl = documentsPath.appendingPathComponent("Sounds")
            let destinationURL = docUrl.appendingPathComponent(audioUrl!.lastPathComponent)
            
            var error: NSError?
            do {
                audioPlayer = try AVAudioPlayer(contentsOf: destinationURL)
            } catch let error1 as NSError {
                error = error1
                audioPlayer = nil
            }
            
            if let err = error {
                print("audioPlayer error \(err.localizedDescription)")
                return
            } else {
                audioPlayer!.delegate = self
                audioPlayer!.prepareToPlay()
            }
            
            //negative number means loop infinity
            audioPlayer!.numberOfLoops = -1
            audioPlayer!.play()
        }
        else{
            let url = URL(fileURLWithPath: Bundle.main.path(forResource: soundName, ofType: "mp3")!)
            var error: NSError?
                       do {
                        audioPlayer = try AVAudioPlayer(contentsOf: url)
                       }
                       catch let error1 as NSError {
                           error = error1
                           audioPlayer = nil
                       }
                       
                       if let err = error {
                           print("audioPlayer error \(err.localizedDescription)")
                           return
                       } else {
                           audioPlayer!.delegate = self
                           audioPlayer!.prepareToPlay()
                       }
                       
                       //negative number means loop infinity
                       audioPlayer!.numberOfLoops = -1
                       audioPlayer!.play()
        }
    }
    
   
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
        //doUpdate()
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        alarmScheduler.checkNotification()

    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


    func setDEfaultLanGerman()
    {
        if(isSelected == "English(UK)"){
            L012Localizer.DoTheSwizzling()
            LocalizedClass.setAppleLAnguageTo(lang: "en-GB")
            
        }else if(isSelected == "German" || isSelected == ""){
            L012Localizer.DoTheSwizzling()
            LocalizedClass.setAppleLAnguageTo(lang: "de")
        }
    }
    
    
    
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        
        // called when user signs into spotify. Session data saved into user defaults, then notification posted to call updateAfterFirstLogin in ViewController.swift. Modeled off recommneded auth flow suggested by Spotify documentation
        if auth.canHandle(auth.redirectURL) {
            
            auth.handleAuthCallback(withTriggeredAuthURL: url, callback: { (error, session) in
                if error != nil {
                    print("error!")
                }
                
                userDef.set(session?.accessToken!, forKey: "access_token")
                
                let sessionData = NSKeyedArchiver.archivedData(withRootObject: session!)
                userDef.set(sessionData, forKey: "SpotifySession")
                userDef.synchronize()
                NotificationCenter.default.post(name: Notification.Name(rawValue: "loginSuccessfull"), object: nil)
            })
            return true
        }
        return false
    }
    
}



//bundle Id
//com.Musify

extension AppDelegate: UNUserNotificationCenterDelegate {
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert])
        
             //   var isSnooze: Bool = false
                var soundName: String = ""
               // var index: Int = -1
             let userInfo = notification.request.content.userInfo
                
                   // isSnooze = userInfo["snooze"] as! Bool
                    soundName = userInfo["soundName"] as! String
                  //  index = userInfo["index"] as! Int
                
                
                var soundID = SystemSoundID()
                 AudioServicesCreateSystemSoundID((URL(fileURLWithPath: soundName) as CFURL), &soundID)
                AudioServicesPlaySystemSound(soundID)
                
                    self.playSound(soundName)
                
                
                //schedule notification for snooze
//                if isSnooze {
//                    let snoozeOption = UIAlertAction(title: "SNOOZE", style: .default) {
//                        (action:UIAlertAction)->Void in self.audioPlayer!.stop()
//                self.alarmScheduler.setNotificationForSnooze(snoozeMinute: 2, soundName: soundName, index: index)
//
//                    }
//                    storageController.addAction(snoozeOption)
//                }
//                let stopOption = UIAlertAction(title: "DISMISS", style: .default) {
//                    (action:UIAlertAction)->Void in self.audioPlayer?.stop()
//                    AudioServicesRemoveSystemSoundCompletion(kSystemSoundID_Vibrate)
//                    self.alarmModel = Alarms()
//                    self.alarmModel.alarms[index].onSnooze = false
//
//                }
                
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        // pull out the buried userInfo dictionary
        let userInfo = response.notification.request.content.userInfo
        let soundName = userInfo["soundName"] as! String
        let index = userInfo["index"] as! Int
        
        switch response.actionIdentifier {
        case UNNotificationDismissActionIdentifier:
            print("Dismiss Action")
        case UNNotificationDefaultActionIdentifier:
            
            self.audioPlayer?.stop()
            let identifier = response.notification.request.identifier
            UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: [identifier])
            
        case Id.snoozeIdentifier:
            
            self.audioPlayer?.stop()
            self.alarmScheduler.setNotificationForSnooze(snoozeMinute: 9, soundName: soundName, index: index)
        case Id.stopIdentifier:
            print("Delete")
            
            self.audioPlayer?.stop()
            let identifier = response.notification.request.identifier
            UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: [identifier])
            break
        default:
            
            print("def")
           // rescheduleRepeating(notification: response.notification)
        }
        
        completionHandler()
    }
    
   
    
}

