//
//  webService.swift
//  Musify
//
//  Created by Rashida on 14/02/20.
//  Copyright © 2020 Rashida. All rights reserved.
//

import Foundation

class webservice{
    
    static func callApiRequestForGet(url : String, completionHandler: @escaping (_ result: Any, _ error: String) -> Void){
        var searchURL = NSURL()
        
        if let url = NSURL(string: "\(url)")
        {
            searchURL = url
        } else {
            
            let Nurl : NSString = url as NSString
            globalFunction.print(object: Nurl)
            let urlStr : NSString = Nurl.addingPercentEscapes(using: String.Encoding.utf8.rawValue)! as NSString
            searchURL = NSURL(string: urlStr as String)!
            globalFunction.print(object: searchURL)
        }
        
        
        
        
        var request = URLRequest(url: searchURL as URL)
        
        request.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
            // Check if data was received successfully
            if error == nil && data != nil {
                do {
                    // Convert NSData to Dictionary where keys are of type String, and values are of any type
                    let json = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! [String:AnyObject]
                    
                    //do your stuff
                    
                    completionHandler(json, "")
                    
                } catch {
                    completionHandler(["":""], "Status_Not_200")
                }
            }
            else if error != nil
            {
                completionHandler(["":""], "error")
            }
        }).resume()
        
        
        
    }
    
    static func onResponsePostData(url: String,parms: [String:String], completion: @escaping (_ res:[String:Any] , _ error : String) -> Void){
        
        let url: URL = URL(string: url)!
        let config = URLSessionConfiguration.default
        config.httpAdditionalHeaders = [
            "Accept" : "application/json",
            "Content-Type" : "application/x-www-form-urlencoded"
        ]
        let session = URLSession(configuration: config)
        var request: URLRequest = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        
        request.encodeParameters(parameters: parms)
        
        let task = session.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {
                // check for fundamental networking error
                globalFunction.print(object: "error=\(String(describing: error))")
                completion(["":""], "\(String(describing: error))")
                return
            }
            do {
                guard let json = (try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers)) as? [String: Any] else {
                    globalFunction.print(object: "Not containing JSON")
                    return
                }
                completion(json , "")
            }
        }
        task.resume()
        
    }
    
    static func callApiRequestForGetWithToken(url : String, completionHandler: @escaping (_ result: Any, _ error: String) -> Void){
          
          var searchURL = NSURL()
          if let url = NSURL(string: "\(url)")
          {
              searchURL = url
          }
          else {
              let Nurl : NSString = url as NSString
              let urlStr : NSString = Nurl.addingPercentEscapes(using: String.Encoding.utf8.rawValue)! as NSString
              searchURL = NSURL(string: urlStr as String)!
          }
          
          var request = URLRequest(url: searchURL as URL)
        
          if let authToken = userDef.value(forKey: "access_token") {
                print(authToken)
            request.httpMethod = "GET"
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue("Bearer \(authToken)", forHTTPHeaderField: "Authorization")

        }
          
          
          URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
            
             if let httpStatus = response as? HTTPURLResponse,
                 
                 httpStatus.statusCode == 401 { // check for httperrors
                 if let parsedData = try? JSONSerialization.jsonObject(with: data!)
                 {
                    print(parsedData)
                 }
             }
            
              // Check if data was received successfully
              if error == nil && data != nil {
                  do {
                      // Convert NSData to Dictionary where keys are of type String, and values are of any type
                     let json = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)
                        completionHandler(json, "")
                        
                    
                      //do your stuff
                      
                  } catch {
                    let json : Any = (Any).self
                    completionHandler(json, "Status_Not_200")
                  }
              }
              else if error != nil
              {
                let json : Any = (Any).self
                 completionHandler(json, "error")
              }
          }).resume()
          
          
          
      }
}

extension URLRequest {
    
    private func percentEscapeString(_ string: String) -> String {
        var characterSet = CharacterSet.alphanumerics
        characterSet.insert(charactersIn: "-._* ")
        
        return string
            .addingPercentEncoding(withAllowedCharacters: characterSet)!
            .replacingOccurrences(of: " ", with: "+")
            .replacingOccurrences(of: " ", with: "+", options: [], range: nil)
    }
    
    mutating func encodeParameters(parameters: [String : String]) {
        httpMethod = "POST"
        
        let parameterArray = parameters.map { (arg) -> String in
            let (key, value) = arg
            return "\(key)=\(self.percentEscapeString(value))"
        }
        
        httpBody = parameterArray.joined(separator: "&").data(using: String.Encoding.utf8)
    }
}
