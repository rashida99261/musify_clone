
//
//  Constants.swift
//  Musify
//
//  Created by Reinforce on 11/02/20.
//  Copyright © 2020 Rashida. All rights reserved.
//

import Foundation

class Constants {
    
    static var arrLang = ["English(UK)", "German"]
    static var AppName = "Musify"
    
    static var search_url = "https://api.spotify.com/v1/search"
    
    static var token_url = "https://accounts.spotify.com/api/token"
    
    static var client_id = "476c620368f349cc8be5b2a29b596eaf"
}




