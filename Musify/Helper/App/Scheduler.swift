//
//  Scheduler.swift
//  Musify
//
//  Created by Apple on 12/02/20.
//  Copyright © 2020 Rashida. All rights reserved.
//

import Foundation
import UIKit
import UserNotifications


class Scheduler : AlarmSchedulerDelegate
{
    var alarmModel: Alarms = Alarms()
    var arrTinme = [String]()
    
    func setupNotificationSettings(){
        
//        var snoozeEnabled: Bool = false
//        if let n = UIApplication.shared.scheduledLocalNotifications {
//            if let result = minFireDateWithIndex(notifications: n) {
//                let i = result.1
//                snoozeEnabled = alarmModel.alarms[i].snoozeEnabled
//            }
//        }
        
        let snoozeAction = UNNotificationAction(identifier: Id.snoozeIdentifier, title: "Snooze", options: [])
        let deleteAction = UNNotificationAction(identifier: Id.stopIdentifier, title: "Dismiss", options: [.destructive])
        let category = UNNotificationCategory(identifier: "myAlarmCategory", actions: [snoozeAction,deleteAction], intentIdentifiers: [], options: [])
        UNUserNotificationCenter.current().setNotificationCategories([category])
        
        // Specify the notification types.
    }
    
    private func correctDate(_ date: Date, onWeekdaysForNotify weekdays:[Int], setTime : String) -> [Date]
    {
        var correctedDate: [Date] = [Date]()
        let calendar = Calendar(identifier: Calendar.Identifier.gregorian)
        let now = Date()
        let flags: NSCalendar.Unit = [NSCalendar.Unit.weekday, NSCalendar.Unit.weekdayOrdinal, NSCalendar.Unit.day]
        var dateComponents = (calendar as NSCalendar).components(flags, from: date)
        dateComponents.timeZone = TimeZone(identifier: "UTC")
        let weekday:Int = dateComponents.weekday!
        
        //no repeat
        if weekdays.isEmpty
        {
            //scheduling date is eariler than current date
            if date < now {
                //plus one day, otherwise the notification will be fired righton
                correctedDate.append((calendar as NSCalendar).date(byAdding: NSCalendar.Unit.day, value: 1, to: date, options:.matchStrictly)!)
            }
            else { //later
                correctedDate.append(date)
            }
            return correctedDate
        }
        //repeat
        else {
            let daysInWeek = 7
            correctedDate.removeAll(keepingCapacity: true)
            for wd in weekdays {
                
                var wdDate: Date!
                //schedule on next week
              //  print("wdd = \(wd)-------\(weekday)")
                if compare(weekday: wd, with: weekday) == .before {
                    wdDate =  (calendar as NSCalendar).date(byAdding: NSCalendar.Unit.day, value: wd+daysInWeek-weekday, to: date, options:.matchStrictly)!
                }
                //schedule on today or next week
                else if compare(weekday: wd, with: weekday) == .same {
                    //scheduling date is eariler than current date, then schedule on next week
                 //   print("date ==== \(date)-----\(now)")
                    
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "hh:mm a"
                    let str1 = dateFormatter.string(from: date)
                    let tm1 = dateFormatter.date(from: str1)
                    let str2 = dateFormatter.string(from: now)
                    let tm2 = dateFormatter.date(from: str2)
                    
                    if tm1!.compare(tm2!) == ComparisonResult.orderedAscending {
                        wdDate = (calendar as NSCalendar).date(byAdding: NSCalendar.Unit.day, value: daysInWeek, to: date, options:.matchStrictly)!
                    }
                    else { //later
                        wdDate = date
                    }
                }
                //schedule on next days of this week
                else { //after
                    wdDate =  (calendar as NSCalendar).date(byAdding: NSCalendar.Unit.day, value: wd-weekday, to: date, options:.matchStrictly)!
                }
                
                //fix second component to 0
                wdDate = Scheduler.correctSecondComponent(date: wdDate, calendar: calendar)
                correctedDate.append(wdDate)
            }
            return correctedDate
        }
    }
    
    public static func correctSecondComponent(date: Date, calendar: Calendar = Calendar(identifier: Calendar.Identifier.gregorian))->Date {
        let second = calendar.component(.second, from: date)
        let d = (calendar as NSCalendar).date(byAdding: NSCalendar.Unit.second, value: -second, to: date, options:.matchStrictly)!
        return d
    }
    
    internal func setNotificationWithDate(_ date: Date, onWeekdaysForNotify weekdays:[Int], snoozeEnabled:Bool,  onSnooze: Bool, soundName: String, index: Int, idemtifier : String) {
        
        let content = UNMutableNotificationContent()
        content.title = NSString.localizedUserNotificationString(forKey: "Alarm!", arguments: nil)
        content.body = NSString.localizedUserNotificationString(forKey: "Wake Up!!", arguments: nil)
        
        if soundName.hasPrefix("https://") { // soundName
            let url = URL(string: soundName)
            let filename = url?.lastPathComponent
            content.sound = UNNotificationSound(named: UNNotificationSoundName(filename!))
        }
        else{
            content.sound = UNNotificationSound(named: UNNotificationSoundName(rawValue: "\(soundName).mp3"))
        }
        
        content.categoryIdentifier = "myAlarmCategory"
        let repeating: Bool = !weekdays.isEmpty
        content.userInfo = ["snooze" : snoozeEnabled, "index": index, "soundName": soundName, "repeating" : repeating]

        //        print(idemtifier)
               
//        if !weekdays.isEmpty && !onSnooze{
//                   AlarmNotification.repeatInterval = NSCalendar.Unit.weekOfYear
//        }
        // find out what weekday is tomorrow

        
//        let notificationTrigger = UNTimeIntervalNotificationTrigger(timeInterval: 2.0, repeats: false)
//        let request = UNNotificationRequest(identifier: "notification", content: content, trigger: notificationTrigger)
//        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
//        let center = UNUserNotificationCenter.current()
//        center.add(request)

       // let triggerDate = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: date)
       // let trigger = UNCalendarNotificationTrigger(dateMatching: triggerDate, repeats: false)

    
        
//        TimeZone(identifier: "UTC")
//
//        let triggerDaily = Calendar.current.dateComponents([.hour, .minute, .second], from: date)
//        let trigger = UNCalendarNotificationTrigger(dateMatching: triggerDaily, repeats: true)
//
//        let identifier = "UYLLocalNotification"
//        let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
//
//        let center = UNUserNotificationCenter.current()
//        center.add(request, withCompletionHandler: { (error) in
//            if let error = error {
//                // Something went wrong
//                print(error)
//            }
//        })

     
//
//        let datesForNotification = correctDate(date, onWeekdaysForNotify:weekdays)
//        print("Curent Date:",datesForNotification)
//        syncAlarmModel()
////
//        for d in datesForNotification {
//            if onSnooze {
//                alarmModel.alarms[index].date = Scheduler.correctSecondComponent(date: alarmModel.alarms[index].date)
//            }
//            else {
//                alarmModel.alarms[index].date = d
//            }
//
//
//        }
//
//        print(alarmModel.alarms[index].date)
//
//        let comps = Calendar.current.dateComponents([.year, .month, .day], from: alarmModel.alarms[index].date)
//        let notificationTrigger = UNCalendarNotificationTrigger(dateMatching: comps, repeats: false)
//
//        OperationQueue.main.addOperation {
//                let request = UNNotificationRequest(identifier: idemtifier, content: content, trigger: notificationTrigger)
//                UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
//                let center = UNUserNotificationCenter.current()
//                center.add(request)
//        }

        var dateComponents = DateComponents()

     //   for i in 0..<alarmModel.count {
            let alarm = alarmModel.alarms[index]
            let dateAsString = alarm.formattedTime
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "hh:mm a"
            let date1 = dateFormatter.date(from: dateAsString)
            dateFormatter.dateFormat = "HH:mm a"
            let Date12 = dateFormatter.string(from: date1!)
          // print("24 hour formatted Date:",Date12)
            let fullTimeArr = Date12.components(separatedBy: " ")
            let strTime = fullTimeArr[0]
             let timeSeprateArr = strTime.components(separatedBy: ":")
            let strHour = timeSeprateArr[0]
            let strMinutes = timeSeprateArr[1]
            
            dateComponents.hour = Int(strHour)
            dateComponents.minute = Int(strMinutes)
        
    //    print("weekdays === \(alarm.repeatWeekdays)")
        
        if alarm.repeatWeekdays.count > 0
        {
            let correctDates = self.correctDate(date, onWeekdaysForNotify: alarm.repeatWeekdays, setTime: Date12)
            
            for day in correctDates //alarm.repeatWeekdays
            {
                //print("nexttttt === \(day)")
                let comps = Calendar.current.dateComponents([.weekday, .hour, .minute, .second], from: day)
                //print("nexttttt === \(comps)")
                
                let trigger = UNCalendarNotificationTrigger(dateMatching: comps, repeats: true)
                let request = UNNotificationRequest(identifier: idemtifier, content: content, trigger: trigger)
                UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
                
                let _ = setupNotificationSettings()
            }
        }
        else {
            let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: true)
            let request = UNNotificationRequest(identifier: idemtifier, content: content, trigger: trigger)
            UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
            
            let _ = setupNotificationSettings()
        }
        
   //     }
     
//        let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: true)
//        let request = UNNotificationRequest(identifier: idemtifier, content: content, trigger: trigger)
//        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
//        let center = UNUserNotificationCenter.current()
//        center.add(request)
        
//        let _ = setupNotificationSettings()
        
    }
    
    func setNotificationForSnooze(snoozeMinute: Int, soundName: String, index: Int) {
        let calendar = Calendar(identifier: Calendar.Identifier.gregorian)
        let now = Date()
        let snoozeTime = (calendar as NSCalendar).date(byAdding: NSCalendar.Unit.minute, value: snoozeMinute, to: now, options:.matchStrictly)!
        setNotificationWithDate(snoozeTime, onWeekdaysForNotify: [Int](), snoozeEnabled: true, onSnooze:true, soundName: soundName, index: index, idemtifier: self.generateRandomString())
    }
    
    func reSchedule() {
        //cancel all and register all is often more convenient
        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
        
        UNUserNotificationCenter.current().getNotificationSettings { (notificationSettings) in
            switch notificationSettings.authorizationStatus {
            case .notDetermined:
                self.requestAuthorization(completionHandler: { (success) in
                    guard success else { return }

                    // Schedule Local Notification
                    self.scheduleLocalNotification()
                })
            case .authorized:
                // Schedule Local Notification
                self.scheduleLocalNotification()
            case .denied:
                print("Application Not Allowed to Display Notifications")
            case .provisional:
                
                print("provisional")
                
             default:
                print("default")
            }
        }
        
        

    }
    
     func scheduleLocalNotification() {
        
        syncAlarmModel()
       // print("alllll = \(alarmModel.count)-----\(alarmModel.alarms)")
        for i in 0..<alarmModel.count {
            
            let alarm = alarmModel.alarms[i]
            
            print(alarm.enabled)
            
            if alarm.enabled {
                setNotificationWithDate(alarm.date as Date, onWeekdaysForNotify: alarm.repeatWeekdays, snoozeEnabled: alarm.snoozeEnabled, onSnooze: false, soundName: alarm.mediaLabel, index: i, idemtifier:self.generateRandomString())
            }
        }
    }
    
     func requestAuthorization(completionHandler: @escaping (_ success: Bool) -> ()) {
        // Request Authorization
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) { (success, error) in
            if let error = error {
                print("Request Authorization Failed (\(error), \(error.localizedDescription))")
            }

            completionHandler(success)
        }
    }
    
    // workaround for some situation that alarm model is not setting properly (when app on background or not launched)
    func checkNotification() {
        alarmModel = Alarms()
        let notifications = [UNMutableNotificationContent()]
        
        if notifications.count > 0{
           for _ in 0..<alarmModel.count {
              //  alarmModel.alarms[i].enabled = false
            }
        }
        else {
            for (i, alarm) in alarmModel.alarms.enumerated() {
                var isOutDated = true
                if alarm.onSnooze {
                    isOutDated = false
                }
                for n in notifications {
//                    if alarm.date >= n.userInfo {
//                        isOutDated = false
//                    }
                }
                if isOutDated {
                   // alarmModel.alarms[i].enabled = false
                }
            }
        }
    }
    
    private func syncAlarmModel() {
        alarmModel = Alarms()
    }
    
    private enum weekdaysComparisonResult {
        case before
        case same
        case after
    }
    
    private func compare(weekday w1: Int, with w2: Int) -> weekdaysComparisonResult
    {
        if w1 != 1 && w2 == 1 {return .before}
        else if w1 == w2 {return .same}
        else {return .after}
    }
    
    private func minFireDateWithIndex(notifications: [UILocalNotification]) -> (Date, Int)? {
        if notifications.isEmpty {
            return nil
        }
        var minIndex = -1
        var minDate: Date = notifications.first!.fireDate!
        for n in notifications {
            let index = n.userInfo!["index"] as! Int
            if(n.fireDate! <= minDate) {
                minDate = n.fireDate!
                minIndex = index
            }
        }
        return (minDate, minIndex)
    }
    
    
     func generateRandomString() -> String{
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let len = UInt32(letters.length)
        var randomStr = ""
        
        for _ in 0 ..< 5{
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomStr += NSString(characters: &nextChar, length: 1) as String
            
        }
        return randomStr
    }
  
}

