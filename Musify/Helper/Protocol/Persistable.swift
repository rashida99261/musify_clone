//
//  Persistable.swift
//  Musify
//
//  Created by Apple on 12/02/20.
//  Copyright © 2020 Rashida. All rights reserved.
//

import Foundation
protocol Persistable{
    var ud: UserDefaults {get}
    var persistKey : String {get}
    func persist()
    func unpersist()
}
