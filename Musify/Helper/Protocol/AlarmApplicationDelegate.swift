//
//  AlarmApplicationDelegate.swift
//  Musify
//
//  Created by Apple on 12/02/20.
//  Copyright © 2020 Rashida. All rights reserved.
//

import Foundation
protocol AlarmApplicationDelegate {
    func playSound(_ soundName: String)
}
