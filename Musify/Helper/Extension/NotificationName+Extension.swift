//
//  NotificationName+Extension.swift
//  Musify
//
//  Created by Apple on 12/02/20.
//  Copyright © 2020 Rashida. All rights reserved.
//

import Foundation

extension NSNotification.Name {
    static let AlarmDisableNotification = NSNotification.Name("AlarmDisableNotification")
}
