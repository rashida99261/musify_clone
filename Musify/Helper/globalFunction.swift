//
//  globalFunction.swift
//  Musify
//
//  Created by Rashida on 08/02/20.
//  Copyright © 2020 Rashida. All rights reserved.
//

import Foundation
import UIKit
import AudioToolbox

class globalFunction {
    
    static func getHeaderImageHeightForCurrentDevice() -> CGFloat {
        switch UIScreen.main.nativeBounds.height {
        case 2436 , 1792 , 2688: // iPhone X,Xr,Xs,Xs max
            return 144
        default: // Every other iPhone
            return 64
        }
    }
    
    static func showLoaderView(view: UIView){
        let spinnerActivity = MBProgressHUD.showAdded(to: view, animated: true);
        spinnerActivity.mode = .indeterminate
        spinnerActivity.isUserInteractionEnabled = false
    }
    
    static func hideLoaderView(view: UIView){
        MBProgressHUD.hide(for: view, animated: true)
    }
    
    static func print( object: Any) {
        Swift.print( object)
    }
    
    static func showAlertMessage(vc: UIViewController, titleStr:String, messageStr:String) -> Void {
        let alert = UIAlertController(title: titleStr, message: messageStr, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.destructive, handler: { action in
           }))
        vc.present(alert, animated: true, completion: nil)
           
           
    }
       
    
       
}


var bundleKey: UInt8 = 0

class AnyLanguageBundle: Bundle {

override func localizedString(forKey key: String,
                              value: String?,
                              table tableName: String?) -> String {

    guard let path = objc_getAssociatedObject(self, &bundleKey) as? String,
        let bundle = Bundle(path: path) else {

            return super.localizedString(forKey: key, value: value, table: tableName)
    }

    return bundle.localizedString(forKey: key, value: value, table: tableName)
  }
}

extension Bundle {

class func setLanguage(_ language: String) {

    defer {

        object_setClass(Bundle.main, AnyLanguageBundle.self)
    }

    objc_setAssociatedObject(Bundle.main, &bundleKey,    Bundle.main.path(forResource: language, ofType: "lproj"), .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
  }
}
