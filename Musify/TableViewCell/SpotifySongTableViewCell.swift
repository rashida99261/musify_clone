//
//  SpotifySongTableViewCell.swift
//  Musify
//
//  Created by Apple on 15/02/20.
//  Copyright © 2020 Rashida. All rights reserved.
//

import UIKit

class SpotifySongTableViewCell: UITableViewCell {
  
    @IBOutlet weak var lblSong: UILabel!
    @IBOutlet weak var imgSong: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var btnPlay : UIButton!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
