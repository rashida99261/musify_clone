//
//  connectViewController.swift
//  Musify
//
//  Created by Reinforce on 11/02/20.
//  Copyright © 2020 Rashida. All rights reserved.
//

import UIKit
import MediaPlayer
import SafariServices
import StoreKit
import MediaPlayer

@_exported import AVFoundation

@available(iOS 10.1, *)
@objcMembers

class connectViewController: UIViewController {

    var auth = SPTAuth.defaultInstance()!
    
    
    var loginUrl: URL?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.setup()
        
         NotificationCenter.default.addObserver(self, selector: #selector(connectViewController.updateAfterFirstLogin), name: NSNotification.Name(rawValue: "loginSuccessfull"), object: nil)
        

        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    @IBAction func clickONBackBtn(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func updateAfterFirstLogin()
    {
        let list = self.storyboard?.instantiateViewController(withIdentifier: "spotifyPlaylistViewController") as! spotifyPlaylistViewController
        self.navigationController?.pushViewController(list, animated: true)
    }
  
}

@available(iOS 10.1, *)

extension connectViewController : SPTAudioStreamingPlaybackDelegate, SPTAudioStreamingDelegate
{
    func setup () {
        // insert redirect your url and client ID below
        let redirectURL = "Spotify-Demo://returnAfterLogin" // put your redirect URL here
        auth.redirectURL     = URL(string: redirectURL)
        auth.clientID        = "476c620368f349cc8be5b2a29b596eaf"
        auth.requestedScopes = [SPTAuthStreamingScope, SPTAuthPlaylistReadPrivateScope, SPTAuthPlaylistModifyPublicScope, SPTAuthPlaylistModifyPrivateScope]
        loginUrl = auth.spotifyWebAuthenticationURL()
        
    }
    
//    func initializaPlayer(authSession:SPTSession){
//        if self.player == nil {
//            self.player = SPTAudioStreamingController.sharedInstance()
//            self.player!.playbackDelegate = self
//            self.player!.delegate = self
//            try! player?.start(withClientId: auth.clientID)
//            self.player!.login(withAccessToken: authSession.accessToken)
//        }
//    }
    
    @IBAction func clickOnBtn(_ sender: UIButton)
    {
        if UIApplication.shared.openURL(loginUrl!) {
            
            if auth.canHandle(auth.redirectURL) {
                // To do - build in error handling
            }
        }
    }
    @IBAction func btnAppleMusicAction(_ sender: UIButton)
    {
       // authenticate()
        
        self.checkPermission()

        
    }
    
    func checkPermission()
    {
        let status = MPMediaLibrary.authorizationStatus()
        switch status {
        case .authorized:
          //  self.openMusicLibrary()
            self.authenticate()
            
            break
        case .notDetermined:
            MPMediaLibrary.requestAuthorization() { status in
                if status == .authorized {
                    DispatchQueue.main.async {
                  //      self.openMusicLibrary()
                        
                        self.authenticate()
                    }
                }
            }
            break
        case .denied:
            //show alert
            print("Please Allow Access to the Media & Apple Music from appliction setting.")
            break
        case .restricted:
            break
        }

    }
    
    
}
//MARK:- Apple Music Code
extension connectViewController{
    private func authenticate() {
        MusicService.authenticate { [weak self] (result) in
            switch result {
            case .success(_): break
            //    self?.resetView()
                
            case .failure(let error):
                self?.handleError(error: error)
            }
        }
    }
    
    private func handleError(error: MusicServiceError) {
        switch error {
        case .authorizationRejected:
            presentServiceSetupView()
        case .userTokenError, .userTokenKeychainError:
            presentAuthenticationError()
        }
    }
    private func presentServiceSetupView() {
           let setupViewController = SKCloudServiceSetupViewController()
        setupViewController.delegate = self as! SKCloudServiceSetupViewControllerDelegate
           
        if #available(iOS 11.0, *) {
            let setupOptions : [SKCloudServiceSetupOptionsKey: Any] = [
                .action: SKCloudServiceSetupAction.subscribe,
                .messageIdentifier: SKCloudServiceSetupMessageIdentifier.join
            ]

            setupViewController.load(options: setupOptions) { [weak self] (didSuceedLoading, error) in
                if didSuceedLoading {
                    DispatchQueue.main.async {
                        self?.present(setupViewController, animated: true, completion: nil)
                    }
                }
            }
        } else {
            // Fallback on earlier versions
        }

           
       }
    
    private func presentAuthenticationError() {
        let alertController = UIAlertController(title: "Error", message: "We are having trouble getting permission to access your  music library. Please try to login again and if you are still having issues please contact support.", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        let supportAction = UIAlertAction(title: "Support", style: .default) { [weak self] (_) in
          //  self?.composeSupportEmail()
        }
        alertController.addAction(supportAction)
        alertController.addAction(okAction)
        
        present(alertController, animated: true, completion: nil)
    }
}
extension connectViewController: SKCloudServiceSetupViewControllerDelegate {
    
    func cloudServiceSetupViewControllerDidDismiss(_ cloudServiceSetupViewController: SKCloudServiceSetupViewController) {
     //   resetView()
    }
}
