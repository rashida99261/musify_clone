//
//  WeckerViewController.swift
//  Musify
//
//  Created by Apple on 10/02/20.
//  Copyright © 2020 Rashida. All rights reserved.
//

import UIKit

class WeckerViewController: UIViewController {
    var alarmDelegate: AlarmApplicationDelegate = AppDelegate()
    var alarmScheduler: AlarmSchedulerDelegate = Scheduler()
    var alarmModel: Alarms = Alarms()

    @IBOutlet weak var tblAlarm: UITableView!
    
    @IBOutlet weak var btnEditAlarm: UIButton!
    
    //var arrAlarm = ["04:10","04:10","04:10","04:10"]
  
}

//MARK:- View LfeCycle
extension WeckerViewController{
    override func viewDidLoad() {
          super.viewDidLoad()
        alarmScheduler.checkNotification()
        tblAlarm.allowsSelectionDuringEditing = true

          // Do any additional setup after loading the view.
      }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func viewWillAppear(_ animated: Bool) {
        alarmModel = Alarms()
        tblAlarm.reloadData()
        //dynamically append the edit button
        if alarmModel.count != 0 {
            btnEditAlarm.isHidden = false
        }
        else {
            btnEditAlarm.isHidden = true
        }
    }
    public func changeSwitchButtonState(index: Int) {
        //let info = notification.userInfo as! [String: AnyObject]
        //let index: Int = info["index"] as! Int
        alarmModel = Alarms()
        if alarmModel.alarms[index].repeatWeekdays.isEmpty {
            alarmModel.alarms[index].enabled = false
        }
        let cells = tblAlarm.visibleCells
        for cell in cells {
            if cell.tag == index {
                let sw = cell.accessoryView as! UISwitch
                if alarmModel.alarms[index].repeatWeekdays.isEmpty {
                    sw.setOn(false, animated: false)
                    cell.backgroundColor = UIColor.groupTableViewBackground
                    cell.textLabel?.alpha = 0.5
                    cell.detailTextLabel?.alpha = 0.5
                }
            }
        }
    }
}
//MARK:- Tableview Delegate Datasource Methords
extension WeckerViewController:UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if alarmModel.count == 0 {
            tblAlarm.separatorStyle = UITableViewCell.SeparatorStyle.none
               }
               else {
            tblAlarm.separatorStyle = UITableViewCell.SeparatorStyle.singleLine
               }
               return alarmModel.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tblAlarm.dequeueReusableCell(withIdentifier: "AlarmTableViewCell") as! AlarmTableViewCell
       
        //cell text
        cell.selectionStyle = .none
        cell.tag = indexPath.row
        let alarm: Alarm = alarmModel.alarms[indexPath.row]
        
        
        cell.lblAlarm.text = alarm.formattedTime
        cell.lblDescription.text = alarm.label
        //append switch button
        let sw = UISwitch(frame: CGRect())
        sw.transform = CGAffineTransform(scaleX: 0.9, y: 0.9);
        
        //tag is used to indicate which row had been touched
        sw.tag = indexPath.row
        sw.addTarget(self, action: #selector(WeckerViewController.switchTapped(_:)), for: .valueChanged)
        if alarm.enabled {
            sw.setOn(true, animated: false)
        }
        cell.accessoryView = sw
        
        //delete empty seperator line
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            if isEditing {
//                performSegue(withIdentifier: Id.editSegueIdentifier, sender: SegueInfo(curCellIndex: indexPath.row, isEditMode: true, label: alarmModel.alarms[indexPath.row].label, mediaLabel: alarmModel.alarms[indexPath.row].mediaLabel, mediaID: alarmModel.alarms[indexPath.row].mediaID, repeatWeekdays: alarmModel.alarms[indexPath.row].repeatWeekdays, enabled: alarmModel.alarms[indexPath.row].enabled, snoozeEnabled: alarmModel.alarms[indexPath.row].snoozeEnabled))
            }
    }
    // Override to support editing the table view.
     func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let index = indexPath.row
            alarmModel.alarms.remove(at: index)
            
            let cells = tableView.visibleCells
            for cell in cells {
                let sw = cell.accessoryView as! UISwitch
                //adjust saved index when row deleted
                if sw.tag > index {
                    sw.tag -= 1
                }
            }
            if alarmModel.count == 0 {
                 btnEditAlarm.isHidden = false
            }
            
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
            alarmScheduler.reSchedule()
        }
    }
}
//MARK:- Uibutton / Switch action
extension WeckerViewController{
    @IBAction func btnAddAlarm(_ sender: Any) {
        tabBarController?.selectedIndex = 0
    }
       
    @objc func switchTapped(_ sender: UISwitch) {
           let index = sender.tag
           alarmModel.alarms[index].enabled = sender.isOn
           if sender.isOn {
               print("switch on")
            alarmScheduler.setNotificationWithDate(alarmModel.alarms[index].date, onWeekdaysForNotify: alarmModel.alarms[index].repeatWeekdays, snoozeEnabled: alarmModel.alarms[index].snoozeEnabled, onSnooze: false, soundName: alarmModel.alarms[index].mediaLabel, index: index, idemtifier: self.generateRandomString())
               tblAlarm.reloadData()
           }
           else {
               print("switch off")
               alarmScheduler.reSchedule()
               tblAlarm.reloadData()
           }
    }
    
    func generateRandomString() -> String{
           
           let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
           let len = UInt32(letters.length)
           var randomStr = ""
           
           for _ in 0 ..< 5{
               let rand = arc4random_uniform(len)
               var nextChar = letters.character(at: Int(rand))
               randomStr += NSString(characters: &nextChar, length: 1) as String
               
           }
           return randomStr
       }
}


