//
//  settingsViewController.swift
//  Musify
//
//  Created by Reinforce on 11/02/20.
//  Copyright © 2020 Rashida. All rights reserved.
//

import UIKit
var isSelected = ""

class settingsViewController: UIViewController {
    
    @IBOutlet weak var txtChngLang : UITextField!
    var pickerViewLanguages: UIPickerView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    

   

}
extension settingsViewController : UIPickerViewDelegate, UIPickerViewDataSource , UITextFieldDelegate{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if(pickerView == pickerViewLanguages){
            return Constants.arrLang.count
        }
       
        else{
            return 0
        }
    }
    
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat
    {
        return 40
    }
    
      func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
      {
         if(pickerView == pickerViewLanguages){
            let item = Constants.arrLang[row]
            return item
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
         if(pickerView == pickerViewLanguages){
            let item = Constants.arrLang[row]
            isSelected = item
            
        }
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {

        if(textField == self.txtChngLang){
            self.pickUp(txtChngLang)
            
        }
        
    }
    
    
    
    func pickUp(_ textField : UITextField) {
        
        
         if(textField == self.txtChngLang){
            self.pickerViewLanguages = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerViewLanguages.delegate = self
            self.pickerViewLanguages.dataSource = self
            self.pickerViewLanguages.backgroundColor = UIColor.white
            textField.inputView = self.pickerViewLanguages
        }

        
        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(settingsViewController.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(settingsViewController.cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    //MARK:- Button
    @objc func doneClick() {
        
        self.txtChngLang.resignFirstResponder()
        
        if(isSelected == "English(UK)"){

            Bundle.setLanguage("en-GB")
            UserDefaults.standard.set(["en-GB"], forKey: APPLE_LANGUAGE_KEY)
            UserDefaults.standard.synchronize()

        }else if(isSelected == "German"){

            Bundle.setLanguage("de")
            UserDefaults.standard.set(["de"], forKey: APPLE_LANGUAGE_KEY)
            UserDefaults.standard.synchronize()
            
           // L012Localizer.DoTheSwizzling()
           // LocalizedClass.setAppleLAnguageTo(lang: "de")
        }
        
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        UIApplication.shared.keyWindow?.rootViewController = storyboard.instantiateInitialViewController()
    }
    
    
    @objc func cancelClick() {
        self.txtChngLang.resignFirstResponder()
    }
    
}
