//  HomeViewController.swift
//  Musify
//
//  Created by Apple on 07/02/20.
//  Copyright © 2020 Rashida. All rights reserved.
//

import UIKit
import Foundation
import MediaPlayer

var curCellIndex: Int = 0
var isEditMode: Bool = false
var mediaID: String = ""
var repeatWeekdays: [Int] = []
var snoozeEnabled: Bool = true

var destinationPath : URL!


@available(iOS 10.1, *)
class HomeViewController: UIViewController {
    
    var alarmScheduler: AlarmSchedulerDelegate = Scheduler()
    var alarmModel: Alarms = Alarms()
    
    var repeatBool : Bool = false
    
    @IBOutlet weak var timePicker : UIDatePicker!
   
    @IBOutlet weak var lblNoWay : UILabel!
    @IBOutlet weak var lblSongName : UILabel!
    
     @IBOutlet weak var imgSongIcon : UIImageView!
     @IBOutlet weak var lblsongTitle : UILabel!
    
    @IBOutlet weak var snoozeSwitch : UISwitch!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.selectedIndex = 0
        self.tabBarController?.tabBar.unselectedItemTintColor = UIColor.darkGray
    }
    
    override func viewWillAppear(_ animated: Bool) {
       // print("repppp bool = \(repeatBool)")
        if (repeatBool == false)
        {
            repeatWeekdays = []
            lblNoWay.text = "No way"
        } else {
            lblNoWay.text = RepeatViewController.repeatText(weekdays: repeatWeekdays)
            repeatBool = false
        }
       // print("reppp = \(repeatWeekdays)")
        self.setUpUI()
        alarmModel=Alarms()
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    func setUpUI()
    {
        timePicker.setValue(UIColor.white, forKeyPath: "textColor")
        timePicker.locale = NSLocale(localeIdentifier: "en_GB") as Locale
        snoozeSwitch.addTarget(self, action: #selector(stateChanged(_:)), for: .valueChanged)
        
        do {
            if let decoded  = UserDefaults.standard.object(forKey: "songDict") as? Data {
                if let userDict = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? [String: Any] {
                    print(userDict)
                    lblSongName.text = "track"
                    let url_str = userDict["imgSong"] as! String
                    let url = URL(string: url_str)
                    self.imgSongIcon.kf.setImage(with: url)
                    self.lblsongTitle.text = userDict["songName"] as? String
                }
            }
            else{
                
//                if let mediaLabel = userDef.value(forKey: "mediaLabel")as? String{
//                    lblSongName.text = mediaLabel
//                    self.lblsongTitle.text = "RingTone"
//                    self.imgSongIcon.image = UIImage(named: "gm_icon")
//                }
//
                
            }
        } catch {
           // Globalfunc.print(object: "Couldn't read file.")
        }
    }
    
    @IBAction func clickONConnectBtn(_ sender: UIButton)
    {
        let connect = self.storyboard?.instantiateViewController(withIdentifier: "connectViewController") as! connectViewController
        self.navigationController?.pushViewController(connect, animated: true)
    }
    
    @IBAction func btnTORepeatAction(_ sender: Any) {
      //  lblNoWay.text = RepeatViewController.repeatText(weekdays: segueInfo.repeatWeekdays)
        repeatBool = true
        let repeatvc = self.storyboard?.instantiateViewController(withIdentifier: "RepeatViewController") as! RepeatViewController
        self.navigationController?.pushViewController(repeatvc, animated: true)

    }
    @IBAction func btnActiveAlarmAction(_ sender: UIButton){
        
        alarmModel.alarms = []
        
        let date = Scheduler.correctSecondComponent(date: timePicker.date)
        let index = curCellIndex
        var tempAlarm = Alarm()
        tempAlarm.date = date
        tempAlarm.label = "Alarm"
        tempAlarm.enabled = true
        
        if let mediaLabel = userDef.value(forKey: "mediaLabel")as? String{
            tempAlarm.mediaLabel = mediaLabel
        }
        
        tempAlarm.mediaID = mediaID
        tempAlarm.snoozeEnabled = snoozeEnabled
        tempAlarm.repeatWeekdays = repeatWeekdays
        tempAlarm.uuid = UUID().uuidString
        tempAlarm.onSnooze = false
        if isEditMode {
            alarmModel.alarms[index] = tempAlarm
        }
        else {
            alarmModel.alarms.append(tempAlarm)
        }
        
        print(alarmModel.alarms)
        
        alarmScheduler.reSchedule()
        self.tabBarController?.selectedIndex = 1
    }
    
    
    @IBAction func btnChooseSongAction(_ sender: UIButton)
    {
        if let songSelectFrom = userDef.value(forKey: "songSelectFrom")as? String{
        
            if(songSelectFrom == "default"){
                let connect = self.storyboard?.instantiateViewController(withIdentifier: "defaultToneViewController") as! defaultToneViewController
                self.navigationController?.pushViewController(connect, animated: true)
            }
            else if(songSelectFrom == "spotify"){
                let connect = self.storyboard?.instantiateViewController(withIdentifier: "connectViewController") as! connectViewController
                self.navigationController?.pushViewController(connect, animated: true)
            }
            
        }
        else{
            let connect = self.storyboard?.instantiateViewController(withIdentifier: "defaultToneViewController") as! defaultToneViewController
            self.navigationController?.pushViewController(connect, animated: true)
        }
        
         
    }
    
   @objc func stateChanged(_ switchState: UISwitch) {
    
        if switchState.isOn {
            snoozeEnabled = true
            print("on")
            
        } else {
            snoozeEnabled = false
            print("off")
        }
    }
    
}
