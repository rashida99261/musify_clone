//
//  defaultToneViewController.swift
//  Musify
//
//  Created by Reinforce on 13/02/20.
//  Copyright © 2020 Rashida. All rights reserved.
//

import UIKit


class defaultToneViewController: UIViewController {
    
    @IBOutlet weak var tblSound : UITableView!
    
    
    
    var arrringTone = ["alarm","bell","morning","tickle"]

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func clickOnBeckBtn(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }

    
}

extension defaultToneViewController : UITableViewDelegate , UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrringTone.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         
        let cell = tblSound.dequeueReusableCell(withIdentifier: "soundTblCell") as! soundTblCell
        cell.lblSoundName.text = arrringTone[indexPath.row]
        
        if let mediaLabel = userDef.value(forKey: "mediaLabel")as? String{
            
            if cell.lblSoundName.text == mediaLabel {
                cell.accessoryType = UITableViewCell.AccessoryType.checkmark
            }
        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        userDef.set("default", forKey: "songSelectFrom")
        userDef.synchronize()
        
        let cell = tblSound.cellForRow(at: indexPath) as! soundTblCell
        cell.accessoryType = UITableViewCell.AccessoryType.checkmark
        
        userDef.set("default", forKey: "songSelectFrom")
        userDef.set(cell.lblSoundName.text!, forKey: "mediaLabel")
        userDef.synchronize()
        
        
        cell.setSelected(true, animated: true)
        cell.setSelected(false, animated: true)
        let cells = tableView.visibleCells
        for c in cells {
            let section = tableView.indexPath(for: c)?.section
            if (section == indexPath.section && c != cell) {
                c.accessoryType = UITableViewCell.AccessoryType.none
            }
        }
    }
    
    
    
}



class soundTblCell : UITableViewCell
{
    @IBOutlet weak var lblSoundName : UILabel!
}
