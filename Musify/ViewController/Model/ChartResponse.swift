//
//  ChartResponse.swift
//  Musify
//
//  Created by Apple on 22/04/20.
//  Copyright © 2020 Rashida. All rights reserved.
//

/**
 Apple Music API - The response to a chart request.
 
 https://developer.apple.com/documentation/applemusicapi/chartresponse
*/

public struct ChartResponse: Codable {
    /**
     The albums returned when fetching charts.
    */
    public let albums: [Chart<Album>]?
    
    /**
     The music videos returned when fetching charts.
    */
    public let musicVideos: [Chart<MusicVideo>]?
    
    /**
     The songs returned when fetching charts.
    */
    public let songs: [Chart<Song>]?
}
