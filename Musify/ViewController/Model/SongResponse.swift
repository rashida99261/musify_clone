//
//  SongResponse.swift
//  Musify
//
//  Created by Apple on 22/04/20.
//  Copyright © 2020 Rashida. All rights reserved.
//

/**
 Apple Music API - The response to a song request.
 
 https://developer.apple.com/documentation/applemusicapi/songresponse
 */

public struct SongResponse: Codable {
    /**
     (Required) The data included in the response for a song object request.
     */
    public let data: [Song]?
}
