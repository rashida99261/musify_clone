//
//  MediaType.swift
//  Musify
//
//  Created by Apple on 22/04/20.
//  Copyright © 2020 Rashida. All rights reserved.
//

public enum MediaType: String, Codable {
    case albums
    case songs
    case artists
    case playlists
    case libraryPlaylists = "library-playlists"
    case librarySongs = "library-songs"
}
