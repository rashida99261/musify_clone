//
//  SearchResults.swift
//  Musify
//
//  Created by Apple on 22/04/20.
//  Copyright © 2020 Rashida. All rights reserved.
//

import Foundation

public struct SearchResults: Codable {
    
    /**
     The songs returned when fetching charts.
     */
    public let songs: SongResponse?
}
