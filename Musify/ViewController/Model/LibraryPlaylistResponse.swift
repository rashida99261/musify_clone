//
//  LibraryPlaylistResponse.swift
//  Musify
//
//  Created by Apple on 22/04/20.
//  Copyright © 2020 Rashida. All rights reserved.
//

/**
The response to a library playlist request.
 
https://developer.apple.com/documentation/applemusicapi/libraryplaylistresponse
 */

public struct LibraryPlaylistResponse: Codable {
    /**
     (Required) The data included in the response for a library playlist object
     */
    public let data: [LibraryPlaylist]?
}
