//
//  Preview.swift
//  Musify
//
//  Created by Apple on 22/04/20.
//  Copyright © 2020 Rashida. All rights reserved.
//

import Foundation

/**
 Apple Music API - An object that represents a preview for resources.
 
 https://developer.apple.com/documentation/applemusicapi/preview
 */
public struct Preview: Codable {
    /**
     The preview artwork for the associated music video.
    */
    public let artwork: Artwork?
    
    /**
     (Required) The preview URL for the content.
     */
    public let url: URL
}

