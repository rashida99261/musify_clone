//
//  RepeatViewController.swift
//  Musify
//
//  Created by Apple on 11/02/20.
//  Copyright © 2020 Rashida. All rights reserved.
//

import UIKit

class RepeatViewController: UIViewController{
    
    @IBOutlet weak var tblRepeat: UITableView!
//    var arrDays = ["Every Sunday","Every Monday","Every Tuesday","Every Wednesday","Every Thursday","Every Friday","Every Saturday"]
    var arrDays = ["Every Sunday","Every Monday","Every Tuesday","Every Wednesday","Every Thursday","Every Friday","Every Saturday"]
    var weekdays = [Int]()

    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
//MARK:- Tableview Delegate/ Datasource
extension RepeatViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrDays.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblRepeat.dequeueReusableCell(withIdentifier: "RepeatTableViewCell")as? RepeatTableViewCell
        cell?.lblDay.text = arrDays[indexPath.row]
        if weekdays.count>0 {
            for weekday in weekdays
            {
                if weekday == (indexPath.row + 1) {
                    cell?.accessoryType = UITableViewCell.AccessoryType.checkmark
                }
            }
        }
        return cell!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tblRepeat.cellForRow(at: indexPath)!
        
        if let index = weekdays.index(of: (indexPath.row + 1)){
            weekdays.remove(at: index)
            cell.setSelected(true, animated: true)
            cell.setSelected(false, animated: true)
            cell.accessoryType = UITableViewCell.AccessoryType.none
        }
        else{
            //row index start from 0, weekdays index start from 1 (Sunday), so plus 1
            weekdays.append(indexPath.row + 1)
            cell.setSelected(true, animated: true)
            cell.setSelected(false, animated: true)
            cell.accessoryType = UITableViewCell.AccessoryType.checkmark
            
        }
    }
}
//MARK:- Custom Methord
extension RepeatViewController{
        static func repeatText(weekdays: [Int]) -> String {
            if weekdays.count == 7 {
                return "Every day"
            }
            
            if weekdays.isEmpty {
                return "Never"
            }
            
            var ret = String()
            var weekdaysSorted:[Int] = [Int]()
            
            weekdaysSorted = weekdays.sorted(by: <)
            
            for day in weekdaysSorted {
                switch day {
                    
                case 2:
                    ret += "Mon "
                case 3:
                    ret += "Tue "
                case 4:
                    ret += "Wed "
                case 5:
                    ret += "Thu "
                case 6:
                    ret += "Fri "
                case 7:
                    ret += "Sat "
                case 1:
                    ret += "Sun "
                default:
                    //throw
                    break
                }
            }
            return ret
        }
    }
//MARK:- Button Action
extension RepeatViewController{
    @IBAction func btnBackAction(_ sender: Any) {
      //  performSegue(withIdentifier: Id.weekdaysUnwindIdentifier, sender: self)
        repeatWeekdays = weekdays
        self.navigationController?.popViewController(animated: true)
}

}
