//
//  spotifySongsViewController.swift
//  Musify
//
//  Created by Rashida on 14/02/20.
//  Copyright © 2020 Rashida. All rights reserved.
//

import UIKit
import SafariServices
@_exported import AVFoundation
import Kingfisher

struct post {
    let mainImage : UIImage!
    let name : String!
    let type : String!
    let struri : String!
    let previewURL : String?
}

class spotifySongsViewController: UIViewController {
    
    @IBOutlet weak var tblSong: UITableView!
    @IBOutlet weak var txtSearchSong: UITextField!
    
    var session:SPTSession!
    var myplaylists = [SPTPartialPlaylist]()
    var searchURL = String()
    
    var playSound: AVAudioPlayer?
    
    var trackUrl = ""
    var arrTrackList : [NSDictionary] = []
    
    var arrBtnTag : NSMutableArray = []
    
  //  var player: SPTAudioStreamingController?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tblSong.tableFooterView = UIView()
        
    
        globalFunction.showLoaderView(view: self.view)
        self.callTrackListApi()
        
    }
    
    
    func initializePlayer()
    {
        
        if player == nil {
             player = SPTAudioStreamingController.sharedInstance()
             player!.playbackDelegate = self
             player!.delegate = self
             try! player?.start(withClientId: Constants.client_id)
             player!.diskCache = SPTDiskCache(capacity: 1024 * 1024 * 64)
                if let authToken = userDef.value(forKey: "access_token") {
                    player!.login(withAccessToken: "\(authToken)")
                }
            }
    }

}

extension spotifySongsViewController:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrTrackList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblSong.dequeueReusableCell(withIdentifier: "SpotifySongTableViewCell") as! SpotifySongTableViewCell
        let dict = self.arrTrackList[indexPath.row]
        print(dict)
        
        let track = dict["track"] as! NSDictionary
        let name = track["name"] as! String
        cell.lblSong.text = name
        
        var arrArtstName = [String]()
        let track_album = track["album"] as! NSDictionary
        let arrArtist = track_album["artists"] as! [NSDictionary]
        
        for i in 0..<arrArtist.count {
            let Adict = arrArtist[i]
            let Art_name = Adict["name"] as! String
            arrArtstName.append(Art_name)
        }
        
         let joined = arrArtstName.joined(separator: ", ")
         cell.lblTitle.text = "\(joined)"
        
         let images = track_album["images"] as! NSArray
         let imgDict = images[0] as! NSDictionary
         let url_str = imgDict["url"] as! String
        
        let url = URL(string: url_str)
        cell.imgSong.kf.setImage(with: url)
        
        
        let btndict = self.arrBtnTag[indexPath.row] as! NSMutableDictionary
        let isplay = btndict["isPlay"] as! Bool
        if(isplay == false){
            cell.btnPlay.isSelected = false
            cell.btnPlay.setImage(#imageLiteral(resourceName: "play"), for: .normal)
        }
        else{
            cell.btnPlay.isSelected = true
            cell.btnPlay.setImage(#imageLiteral(resourceName: "pause"), for: .normal)
        }
        
        cell.btnPlay.tag = indexPath.row
        cell.btnPlay.addTarget(self, action: #selector(btnPlaySongAction(_:)), for: UIControl.Event.touchUpInside)

        return cell
    }
    
    
}

//MARK:- Audio Play
extension spotifySongsViewController: SPTAudioStreamingPlaybackDelegate, SPTAudioStreamingDelegate{
    
    func audioStreamingDidLogin(_ audioStreaming: SPTAudioStreamingController!) {
    }
    func audioStreaming(_ audioStreaming: SPTAudioStreamingController!, didReceiveError error: Error!) {
        print(error)
    }
    
    func downloadFileFromURL(url: URL){
        var downloadTask = URLSessionDownloadTask()
        downloadTask = URLSession.shared.downloadTask(with: url, completionHandler: {
            customURL, response, error in
            self.play(url: customURL!)
        })
        downloadTask.resume()
    }
    
    func play(url: URL) {
        do {
            playSound = try AVAudioPlayer(contentsOf: url)
            playSound!.prepareToPlay()
            playSound!.play()
        }
        catch{
            print(error)
        }
    }
    
}
extension spotifySongsViewController{
    
    @IBAction func clickOnBeckBtn(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @objc func btnPlaySongAction(_ sender: UIButton) {
        let buttonPostion = sender.convert(sender.bounds.origin, to: tblSong)
        if let indexPath = tblSong.indexPathForRow(at: buttonPostion) {
            let dict = self.arrTrackList[indexPath.row]
            let track = dict["track"] as! NSDictionary
            if let preview_url = track["preview_url"] as? String{
                let dict = self.arrBtnTag[indexPath.row] as! NSMutableDictionary
                let isplay = dict["isPlay"] as! Bool
                if(isplay == false){
                 //   sender.isSelected = true
                    sender.setImage(#imageLiteral(resourceName: "pause"), for: .normal)
                    downloadFileFromURL(url: URL(string: preview_url)!)
                    playSound?.play()
                    
                     for i in 0..<self.arrBtnTag.count{
                        let tag_dict = self.arrBtnTag[i] as! NSMutableDictionary
                         if(i == indexPath.row){
                            tag_dict.setValue(true, forKey: "isPlay")
                            arrBtnTag.replaceObject(at:i, with: tag_dict)
                         }
                        else{
                            tag_dict.setValue(false, forKey: "isPlay")
                            arrBtnTag.replaceObject(at: i, with: tag_dict)
                        }
                     }
                    self.tblSong.reloadData()
                }
                else{
                    sender.isSelected = false
                    sender.setImage(#imageLiteral(resourceName: "play"), for: .normal)
                    playSound?.pause()
                    
                    for i in 0..<self.arrBtnTag.count{
                        let tag_dict = self.arrBtnTag[i] as! NSMutableDictionary
                            if(i == indexPath.row){
                                tag_dict.setValue(false, forKey: "isPlay")
                                arrBtnTag.replaceObject(at:i, with: tag_dict)
                            }
                            else{
                                tag_dict.setValue(false, forKey: "isPlay")
                                arrBtnTag.replaceObject(at: i, with: tag_dict)
                            }
                        }
                    self.tblSong.reloadData()
                }
            }
            else{
                
                let uri = track["uri"] as! String
                
                if sender.isSelected{
                    sender.isSelected = false
                    sender.setImage(#imageLiteral(resourceName: "play"), for: .normal)
                    
                    player?.setIsPlaying(false, callback: { (error) in
                    })
                    
                    
                }else{
                    sender.isSelected = true
                    sender.setImage(#imageLiteral(resourceName: "pause"), for: .normal)
                    player?.playSpotifyURI(uri, startingWith: 0, startingWithPosition: 0, callback: { (error) in
                    })
                }
                
                
            }
            
        }
    }
    
    @IBAction func btnSetAlarmAction(_ sender: UIButton) {
                
        let buttonPostion = sender.convert(sender.bounds.origin, to: tblSong)
        
        if let indexPath = tblSong.indexPathForRow(at: buttonPostion) {
            let rowIndex =  indexPath.row
            print(rowIndex)
            
            let dict = self.arrTrackList[indexPath.row]
                       
            let track = dict["track"] as! NSDictionary
            let name = track["name"] as! String
            
            let track_album = track["album"] as! NSDictionary
            let images = track_album["images"] as! NSArray
            let imgDict = images[0] as! NSDictionary
            let url_str = imgDict["url"] as! String

            if let preview_url = track["preview_url"] as? String{
                globalFunction.showLoaderView(view: self.view)
                if let audioUrl = URL(string: preview_url) {
                    let urlSession = URLSession(configuration: .default, delegate: self, delegateQueue: OperationQueue())
                    let downloadTask = urlSession.downloadTask(with: audioUrl)
                    downloadTask.resume()
                    let dict:[String:Any] = ["songName":name,"imgSong":url_str, "from":"spotify"]
                    let userDefaults = UserDefaults.standard
                    do {
                        if #available(iOS 11.0, *) {
                            let myData = try NSKeyedArchiver.archivedData(withRootObject: dict, requiringSecureCoding: false)
                            userDefaults.set(myData, forKey: "songDict")

                        } else {
                            // Fallback on earlier versions
                        }
                   
                    } catch {
                        // Globalfunc.print(object: "Couldn't write file")
                    }
                }
            }
            else{
                
            }
        }
    }
}


extension spotifySongsViewController:  URLSessionDownloadDelegate {

    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        print("downloadLocation:", location)
        // create destination URL with the original pdf name
        guard let url = downloadTask.originalRequest?.url else { return }
        
        let _ = try? FileManager.default.soundsLibraryURL(for: url.lastPathComponent)
      //  print(targetURL)

        let documentsPath = FileManager.default.urls(for: .libraryDirectory, in: .userDomainMask)[0]
        
        let docUrl = documentsPath.appendingPathComponent("Sounds")
        let destinationURL = docUrl.appendingPathComponent(url.lastPathComponent)
        // delete original copy
        try? FileManager.default.removeItem(at: destinationURL)
        // copy from temp to Document
        do {
            try FileManager.default.copyItem(at: location, to: destinationURL)
            let pdfURL = destinationURL
            
            print(pdfURL)
            
            OperationQueue.main.addOperation {
                
                globalFunction.hideLoaderView(view: self.view)
                
                userDef.set("\(pdfURL)", forKey: "mediaLabel")
                userDef.synchronize()
                
                let home = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                userDef.set("spotify", forKey: "songSelectFrom")
                userDef.synchronize()
                self.navigationController?.pushViewController(home, animated: true)
            }
        } catch let error {
            print("Copy Error: \(error.localizedDescription)")
        }
    }
}

extension FileManager {

    func soundsLibraryURL(for filename: String) throws -> URL {
        let libraryURL = try url(for: .libraryDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        let soundFolderURL = libraryURL.appendingPathComponent("Sounds", isDirectory: true)
        if !fileExists(atPath: soundFolderURL.path) {
            try createDirectory(at: soundFolderURL, withIntermediateDirectories: true)
        }
        return soundFolderURL.appendingPathComponent(filename, isDirectory: false)
    }
}

extension spotifySongsViewController {
    
    func callTrackListApi(){
        
        let url = "\(trackUrl)?offset=0&limit=50"
                webservice.callApiRequestForGetWithToken(url: url) { (dict, error) in
                    
                    if(error == ""){
                        OperationQueue.main.addOperation {
                            globalFunction.hideLoaderView(view: self.view)
                            if let respDict = dict as? NSDictionary{
                                if let items = respDict["items"] as? [NSDictionary]{
                                    print(items)
                                    self.arrTrackList = items
                                    
                                    for _ in 0..<self.arrTrackList.count{
                                        let dict : NSMutableDictionary = [:]
                                        dict.setValue(false, forKey: "isPlay")
                                        self.arrBtnTag.add(dict)
                                    }
                                    
                                    self.tblSong.reloadData()
                                }
                            }
                        }
                    }
                    else{
                        OperationQueue.main.addOperation {
                            globalFunction.showAlertMessage(vc: self, titleStr: "", messageStr: "")
                        }
                    }
        }
    }
}


