//
//  spotifyPlaylistViewController.swift
//  Musify
//
//  Created by Apple on 18/04/20.
//  Copyright © 2020 Rashida. All rights reserved.
//

import UIKit
import Kingfisher

class spotifyPlaylistViewController: UIViewController {
    
    @IBOutlet weak var tblPlayList : UITableView!
    @IBOutlet weak var txtSearch : UITextField!
    
    var arrPlayList : [NSDictionary] = []
    var arrFilterData : [NSDictionary] = []
    
    var searchActive : Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.tblPlayList.tableFooterView = UIView()
        self.getCurrentUserSongPlayList()
    }
    
    @IBAction func clickOnBeckBtn(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
}

extension spotifyPlaylistViewController : UITableViewDelegate , UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(searchActive) {
            return arrFilterData.count
        }
        return arrPlayList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblPlayList.dequeueReusableCell(withIdentifier: "tblPlaylistCell") as! tblPlaylistCell
        
        var dict : NSDictionary!
        if(searchActive){
            dict = arrFilterData[indexPath.row]
        }else{
            dict = arrPlayList[indexPath.row]
        }
        
        print(dict)
        let playname = dict["name"] as! String
        cell.lblPlaylist_name.text = playname
        
         let owner = dict["owner"] as! NSDictionary
         let display_name = owner["display_name"] as! String
         cell.lblCreateby.text = display_name
        
         let images = dict["images"] as! NSArray
         let imgDict = images[0] as! NSDictionary
         let url_str = imgDict["url"] as! String
        
        let url = URL(string: url_str)
        cell.imgPlay.kf.setImage(with: url)

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        var dict : NSDictionary!
        if(searchActive){
            dict = arrFilterData[indexPath.row]
        }else{
            dict = arrPlayList[indexPath.row]
        }
        
        let tracks = dict["tracks"] as! NSDictionary
        let href = tracks["href"] as! String
        
        
        let list = self.storyboard?.instantiateViewController(withIdentifier: "spotifySongsViewController") as! spotifySongsViewController
        list.trackUrl = href
        self.navigationController?.pushViewController(list, animated: true)
        
    }
}

class tblPlaylistCell: UITableViewCell {
    
    @IBOutlet weak var imgPlay : UIImageView!
    @IBOutlet weak var lblPlaylist_name : UILabel!
    @IBOutlet weak var lblCreateby : UILabel!
}

extension spotifyPlaylistViewController {
    
    func getCurrentUserSongPlayList(){
        
        let url = "https://api.spotify.com/v1/me/playlists"
        webservice.callApiRequestForGetWithToken(url: url) { (dict, error) in
            
            if(error == ""){
                OperationQueue.main.addOperation {
                    
                    if let respDict = dict as? NSDictionary{
                        if let items = respDict["items"] as? [NSDictionary]{
                            print(items)
                            self.arrPlayList = items
                            self.tblPlayList.reloadData()
                        }
                    }
                }
            }
            else{
                OperationQueue.main.addOperation {
                    globalFunction.showAlertMessage(vc: self, titleStr: "", messageStr: "")
                }
            }

        }
    }
}

extension spotifyPlaylistViewController : UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        searchActive = true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        searchActive = false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if (string.count == 0){
            
            searchActive = false
            tblPlayList.reloadData()
        }
        else{
              searchActive = true
              arrFilterData.removeAll()
             
              for i in 0...arrPlayList.count-1{
             
                  let dict = arrPlayList[i]
                  let tmp = dict["name"] as! NSString
                 let range = tmp.range(of: string, options: NSString.CompareOptions.caseInsensitive)
             
                  if(range.location != NSNotFound){
                      arrFilterData.append(dict)
                  }
              }
        }
        tblPlayList.reloadData()
        return true
    }
    
}
