//
//  Constants.swift
//  Musify
//
//  Created by Apple on 22/04/20.
//  Copyright © 2020 Rashida. All rights reserved.
//

import Foundation

public extension Notification.Name {
    static let sessionStatusChanged = Notification.Name("SessionStatusChanged")
}

public struct ConstantsApple {
    public enum AppleMusic : String {
         case developerToken = "eyJhbGciOiJFUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IlgySjc5WUZDTDcifQ.eyJpc3MiOiJDRkM1OEE5WUdHIiwiaWF0IjoxNTg3MzgxNTYxLCJleHAiOjE2MDMxNDk1NjF9.4nI3S9SFVo2WMlV77ehq8qXi9I9sNPwh8_OTDzZnNUi_Olnc6Asp1uU_s6XC_ehS29xq6zcQ1gzCysWehNPTKw"
    }
    public enum Keychain : String, Codable {
        case appleMusicUserToken
    }
}
