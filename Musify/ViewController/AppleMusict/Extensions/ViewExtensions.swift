//
//  ViewExtensions.swift
//  Musify
//
//  Created by Apple on 22/04/20.
//  Copyright © 2020 Rashida. All rights reserved.
//

import UIKit

extension UIView {
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
        }
    }
    
    func constrainToBounds(otherView: UIView) {
        self.leadingAnchor.constraint(equalTo: otherView.leadingAnchor).isActive = true
        self.trailingAnchor.constraint(equalTo: otherView.trailingAnchor).isActive = true
        self.topAnchor.constraint(equalTo: otherView.topAnchor).isActive = true
        self.bottomAnchor.constraint(equalTo: otherView.bottomAnchor).isActive = true
    }
    
    func addDropshadow() {
        layer.shadowColor = UIColor.red.cgColor
        layer.shadowRadius = 6.0
        layer.shadowOpacity = 1.0
    }
}

